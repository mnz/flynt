import sys
from dataclasses import dataclass
from datetime import date, datetime, time, timedelta
from decimal import Decimal
from typing import Dict, List, Optional, Union

import pyspark.sql.types as T
import pytest

from flynt import schema_for
from flynt._schema import _infer_spark_type, _unwrap_optional_type
from flynt.types import Byte, Double, Long, Short


@pytest.mark.parametrize(
    ("data_type", "expected"),
    (
        (int, T.IntegerType()),
        (Byte, T.ByteType()),
        (Short, T.ShortType()),
        (Long, T.LongType()),
        (float, T.FloatType()),
        (Double, T.DoubleType()),
        (Decimal, T.DecimalType(precision=38, scale=18)),
        (str, T.StringType()),
        (bytes, T.BinaryType()),
        (bytearray, T.BinaryType()),
        (bool, T.BooleanType()),
        (datetime, T.TimestampType()),
        (time, T.TimestampType()),
        (date, T.DateType()),
        (timedelta, T.DayTimeIntervalType()),
    ),
)
def test_infer_spark_type_primitive(data_type: type, expected: T.DataType) -> None:
    assert _infer_spark_type(data_type) == expected


@pytest.mark.parametrize(
    ("data_type", "expected"),
    (
        (
            List[int],
            T.ArrayType(T.IntegerType(), False),
        ),
        (
            List[List[str]],
            T.ArrayType(T.ArrayType(T.StringType(), False), False),
        ),
        (
            Dict[str, int],
            T.MapType(T.StringType(), T.IntegerType(), False),
        ),
        (
            Dict[str, List[int]],
            T.MapType(T.StringType(), T.ArrayType(T.IntegerType(), False), False),
        ),
    ),
)
def test_infer_spark_type_composite(data_type: type, expected: T.DataType) -> None:
    assert _infer_spark_type(data_type) == expected


@pytest.mark.skipif(
    condition=sys.version_info < (3, 9),
    reason="Python < 3.9 does not support generics in standard collections",
)
@pytest.mark.parametrize(
    ("data_type", "expected"),
    (
        (
            "list[int]",
            T.ArrayType(T.IntegerType(), False),
        ),
        (
            "list[List[str]]",
            T.ArrayType(T.ArrayType(T.StringType(), False), False),
        ),
        (
            "dict[str, int]",
            T.MapType(T.StringType(), T.IntegerType(), False),
        ),
        (
            "dict[str, List[int]]",
            T.MapType(T.StringType(), T.ArrayType(T.IntegerType(), False), False),
        ),
    ),
)
def test_infer_spark_type_composite_pep585(
    data_type: str, expected: T.DataType
) -> None:
    assert _infer_spark_type(eval(data_type)) == expected


@pytest.mark.parametrize(
    ("data_type", "expected"),
    (
        (Optional[int], int),
        (Optional[List[int]], List[int]),
        (Union[str, None], str),
        (Dict[str, int], None),
        (None, None),
    ),
)
def test_unwrap_optional_type(data_type: type, expected: Optional[type]) -> None:
    assert _unwrap_optional_type(data_type) == expected


@pytest.mark.skipif(
    condition=sys.version_info < (3, 10),
    reason="Python < 3.10 does not support writing union types as X | Y",
)
@pytest.mark.parametrize(
    ("data_type", "expected"),
    (
        ("int | None", "int"),
        ("list[int] | None", "list[int]"),
        ("str | None", "str"),
        ("dict[str, int]", "None"),
        ("None", "None"),
    ),
)
def test_unwrap_optional_type_pep604(data_type: str, expected: str) -> None:
    assert _unwrap_optional_type(eval(data_type)) == eval(expected)


def test_schema_for() -> None:
    @dataclass
    class Primitive:
        a: Optional[float]
        b: str
        c: timedelta

    primitive_schema = T.StructType(
        [
            T.StructField(name="a", dataType=T.FloatType(), nullable=True),
            T.StructField(name="b", dataType=T.StringType(), nullable=False),
            T.StructField(name="c", dataType=T.DayTimeIntervalType(), nullable=False),
        ]
    )
    assert schema_for(Primitive) == primitive_schema

    @dataclass
    class Composite:
        d: List[Optional[int]]
        e: Dict[str, bytes]

    composite_schema = T.StructType(
        [
            T.StructField(
                name="d",
                dataType=T.ArrayType(T.IntegerType(), True),
                nullable=False,
            ),
            T.StructField(
                name="e",
                dataType=T.MapType(T.StringType(), T.BinaryType(), False),
                nullable=False,
            ),
        ]
    )
    assert schema_for(Composite) == composite_schema

    @dataclass
    class Mixed:
        f: List[Dict[str, Primitive]]
        g: Dict[Primitive, Optional[List[Dict[int, Composite]]]]

    mixed_schema = T.StructType(
        [
            T.StructField(
                "f",
                T.ArrayType(
                    T.MapType(
                        T.StringType(),
                        T.StructType(
                            [
                                T.StructField("a", T.FloatType(), True),
                                T.StructField("b", T.StringType(), False),
                                T.StructField("c", T.DayTimeIntervalType(0, 3), False),
                            ]
                        ),
                        False,
                    ),
                    False,
                ),
                False,
            ),
            T.StructField(
                "g",
                T.MapType(
                    T.StructType(
                        [
                            T.StructField("a", T.FloatType(), True),
                            T.StructField("b", T.StringType(), False),
                            T.StructField("c", T.DayTimeIntervalType(0, 3), False),
                        ]
                    ),
                    T.ArrayType(
                        T.MapType(
                            T.IntegerType(),
                            T.StructType(
                                [
                                    T.StructField(
                                        "d",
                                        T.ArrayType(T.IntegerType(), True),
                                        False,
                                    ),
                                    T.StructField(
                                        "e",
                                        T.MapType(
                                            T.StringType(), T.BinaryType(), False
                                        ),
                                        False,
                                    ),
                                ]
                            ),
                            False,
                        ),
                        False,
                    ),
                    True,
                ),
                False,
            ),
        ]
    )
    assert schema_for(Mixed) == mixed_schema
