# Flynt

## About
`Flynt` is a small Python library for converting dataclasses to Spark schemas.

## Example
Define your dataclasses, like you would for any Python program:
```python
from dataclasses import dataclass
from typing import List, Optional

import flynt

@dataclass
class Section:
    heading: str
    images: Optional[List[str]]

@dataclass
class Page:
    item_id: str
    page_title: str
    wiki_db: str
    sections: List[Section]
```
Get a Spark schema for your dataclass using `Flynt`:
```python
flynt.schema_for(Page)
```
```
root
 |-- item_id: string (nullable = false)
 |-- page_title: string (nullable = false)
 |-- wiki_db: string (nullable = false)
 |-- sections: array (nullable = false)
 |    |-- element: struct (containsNull = false)
 |    |    |-- heading: string (nullable = false)
 |    |    |-- images: array (nullable = true)
 |    |    |    |-- element: string (containsNull = false)
```
