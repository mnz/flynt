import sys
import types
from dataclasses import fields, is_dataclass
from datetime import date, datetime, time, timedelta
from decimal import Decimal
from typing import (
    Any,
    Dict,
    List,
    Optional,
    Set,
    Type,
    Union,
    get_args,
    get_origin,
    get_type_hints,
)

import pyspark.sql.types as T

from flynt.types import Byte, Double, Long, NoneType, Short


def _unwrap_optional_type(data_type: type) -> Optional[type]:
    """Get the inner type of an optional type.

    Unwraps both `Optional` and `Union` where `Union` can
    only have two variants, one of which should be `None`.
    Returns `None` if the type is not an optional.
    """
    data_type_origin = get_origin(data_type)
    is_union = data_type_origin is Union or (
        # Python < 3.10 does not have UnionType e.g. str | None
        sys.version_info >= (3, 10) and data_type_origin is types.UnionType
    )
    if is_union:
        union_args: Set[type] = set(get_args(data_type))
        is_optional_type = len(union_args) == 2 and NoneType in union_args
        if not is_optional_type:
            raise TypeError(f"Unsupported field type: {data_type}")
        return (union_args - {NoneType}).pop()

    return None


def _infer_spark_array_type(list_type: Type[List[Any]]) -> T.ArrayType:
    [list_arg] = get_args(list_type)
    contains_null = False

    unwrapped_list_arg = _unwrap_optional_type(list_arg)
    if unwrapped_list_arg:
        list_arg = unwrapped_list_arg
        contains_null = True

    return T.ArrayType(
        elementType=_infer_spark_type(list_arg),
        containsNull=contains_null,
    )


def _infer_spark_map_type(dict_type: Type[Dict[Any, Any]]) -> T.MapType:
    [key_type, value_type] = get_args(dict_type)
    is_value_nullable = False

    if _unwrap_optional_type(key_type):
        raise TypeError(f"Invalid key type: {key_type}")

    unwrapped_value_type = _unwrap_optional_type(value_type)
    if unwrapped_value_type:
        value_type = unwrapped_value_type
        is_value_nullable = True

    return T.MapType(
        keyType=_infer_spark_type(key_type),
        valueType=_infer_spark_type(value_type),
        valueContainsNull=is_value_nullable,
    )


def _infer_spark_type(data_type: type) -> T.DataType:
    if data_type is int:
        return T.IntegerType()
    elif data_type is Byte:
        return T.ByteType()
    elif data_type is Short:
        return T.ShortType()
    elif data_type is Long:
        return T.LongType()
    elif data_type is float:
        return T.FloatType()
    elif data_type is Double:
        return T.DoubleType()
    elif data_type is Decimal:
        return T.DecimalType(38, 18)
    elif data_type is str:
        return T.StringType()
    elif data_type in (bytes, bytearray):
        return T.BinaryType()
    elif data_type is bool:
        return T.BooleanType()
    elif data_type is datetime:
        return T.TimestampType()
    elif data_type is time:
        return T.TimestampType()
    elif data_type is date:
        return T.DateType()
    elif data_type is timedelta:
        return T.DayTimeIntervalType()
    elif get_origin(data_type) in (list, List):
        return _infer_spark_array_type(data_type)
    elif get_origin(data_type) in (dict, Dict):
        return _infer_spark_map_type(data_type)
    elif is_dataclass(data_type):
        return schema_for(data_type)
    else:
        raise TypeError(f"Unsupported type: {data_type}")


def _make_struct_field(name: str, data_type: type) -> T.StructField:
    nullable = False

    unwrapped_data_type = _unwrap_optional_type(data_type)
    if unwrapped_data_type is not None:
        data_type = unwrapped_data_type
        nullable = True

    return T.StructField(
        name=name,
        dataType=_infer_spark_type(data_type),
        nullable=nullable,
    )


def schema_for(cls: type) -> T.StructType:
    """Generate a spark schema for the given dataclass.

    Converts a dataclass to a spark `StructType` by inferring
    spark `DataType` from the dataclass field type hints and then
    making `StructField` from the inferred types.
    """
    if not is_dataclass(cls):
        raise ValueError(f"{cls} is not a dataclass")

    # dataclass.fields does not resolve string type annotations
    # like "Foo" to the actual type for performance reasons.
    resolved_field_types = get_type_hints(cls)

    struct_fields = [
        _make_struct_field(
            name=field.name,
            data_type=resolved_field_types[field.name],
        )
        for field in fields(cls)
    ]
    return T.StructType(struct_fields)
