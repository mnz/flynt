from typing import NewType, Type

NoneType: Type[None] = type(None)

Byte = NewType("Byte", int)
Short = NewType("Short", int)
Long = NewType("Long", int)

Double = NewType("Double", float)
